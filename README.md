# TP Docker

Ce projet est un projet qui nous a été donné par notre intervenant Mr Jules AGOSTINI dans le cadre de notre formation en Mastère 1 Expert Cloud Sécurité & Infrastructure à Nantes Ynov Campus.

## I - L'équipe
- Erwan BATTAIS   
M1 Expert Cloud Sécurité & Infrastructure @ Nantes Ynov Campus   
Alternant technicien Infrastructure & SI @ NET4BUSINESS   
- Camille GUERIN   
M1 Expert Cloud Sécurité & Infrastructure @ Nantes Ynov Campus   
Alternant Système et Réseau @ CentralCom Division Entreprise   
- David MICHON   
M1 Expert Cloud Sécurité & Infrastructure @ Nantes Ynov Campus   
Administrateur réseau et systèmes @ Apside   

## II - Schéma du projet
![Schéma](/img/schema.jpg)

## III - Les composants du projet
Notre projet est composé de plusieurs éléments :
- une [voting-app];
- un reverse proxy, [Traefik];
- un container [SonarQube];
- un container [Harbor];
- une solution de monitoring, [Prometheus] & [Grafana];
- une solution de sécurité, [Dockle] & [Trivy];

### Présentation des différents composants
- [Traefik]   
Traefik est un reverse-proxy et load-balancer HTTP et TCP open-source. La gestion des certificats SSL est aussi de la partie et est « auto-gérée » par Let’s Encrypt dans le cadre de notre projet.
- [SonarQube]   
SonarQube est un outil (Open Source) qui permet de tester et mesurer la qualité du code d’un projet informatique. Compatible avec un grand nombre de langages, il est la garantie qualité de notre projet.
- [Harbor]   
Harbor est un des produits open sourcé par VMware. C’est un serveur permettant de stocker « on premise » ou dans le cloud des images Docker, au même titre que le produit historiquement appelé Docker Registry (maintenant Docker Distribution), mais avec des fonctionnalités supplémentaires. [Plus de détails sur Harbor]
- [Prometheus] & [Grafana]   
Prometheus est une solution open source, conçue pour récupérer les métriques de fonctionnement des serveurs et créer une gestion d’alertes en fonction de seuils considérés critiques. Cet outil de monitoring enregistre en temps réel ces données à l'aide d'un modèle d'extraction de endpoints HTTP.   
Grafana est une plateforme open source taillée pour la surveillance, l'analyse et la visualisation des métriques.   
En mêlant Prometheus (récupération des données brutes) et Grafana (affichage graphique des données) nous pouvons facilement garder un oeil sur notre application.
- [Dockle] & [Trivy]   
Dockle est un outil permettant à la fois de vérifier l’application des bonnes pratiques définies par Docker Inc. ainsi que de faire un audit sécurité d’une image. Se basant sur l’image buildée et non sur le simple Dockerfile, l’outil présente l’avantage de détecter des transgressions de manière plus poussée qu’au travers d’un simple lint du Dockerfile.   
Trivy est un scanner de vulnérabilité pour les conteneurs qui se veut simple et complet. Il va détecter les vulnérabilités des packages OS (Alpine, RHEL, CentOS, etc.) et les dépendances des applications (Bundler, Composer, npm, yarn etc.).   
L'utilisation de Dockle et Trivy nous permet de nous assurer que notre projet est bien sécurisé (image saine et vulnérabilités détectées).

## IV - Installation
Pour lancer notre projet vous aurez besoin d'installer [Docker] ainsi que [docker-compose] (v1.28.0 minimum) sur votre machine.

### Lancement des différents éléments
Pour lancer le projet vous aurez besoin de vous positionner dans le répertoire traefik et d'utiliser la commande suivante qui permettra de démarrer le reverse-proxy :
```sh
# docker-compose up -d
```
Il faudra ensuite se placer dans le répertoire voting-app et effectuer la même opération pour lancer la voting-app :
```sh
# docker-compose up -d
```

## V - Liens-utiles
- https://traefik.me/
- https://www.grottedubarbu.fr/tag/traefik/
- https://yoandev.co/du-https-en-local-avec-docker-traefik-traefik-me-et-lets-encrypt

## VI - Sources
- https://computerz.solutions/docker-compose-traefik/
- https://www.webandco2-0.fr/actualites/article/SonarQube/99
- https://blog.zwindler.fr/2018/02/27/harbor-la-docker-registry-dentreprise-open-source-de-vmware-part-1/
- https://www.journaldunet.fr/web-tech/guide-de-l-entreprise-digitale/1443882-grafana-la-data-visualisation-du-monitoring-it-open-source-gratuit/
- https://www.journaldunet.fr/web-tech/guide-de-l-entreprise-digitale/1443880-prometheus-le-monitoring-oriente-alerting-open-source-gratuit/
- https://blog.octo.com/panorama-des-outils-de-securite-autour-des-conteneurs/
- https://medium.com/ouidou/un-monitoring-complet-en-quelques-minutes-avec-prometheus-33e849e6392e

[Docker]: <https://docs.docker.com/get-docker/>
[docker-compose]: <https://docs.docker.com/compose/install/>
[voting-app]: <https://github.com/dockersamples/example-voting-app>
[Traefik]: <https://doc.traefik.io/traefik/>
[SonarQube]: <https://docs.sonarqube.org/latest/>
[Harbor]: <https://github.com/goharbor/harbor>
[Prometheus]: <https://prometheus.io/docs/prometheus/latest/installation/>
[Grafana]: <https://grafana.com/docs/grafana/latest/getting-started/>
[Dockle]: <https://github.com/goodwithtech/dockle>
[Trivy]: <https://github.com/aquasecurity/trivy>
[Plus de détails sur Harbor]: <https://blog.zwindler.fr/2018/02/27/harbor-la-docker-registry-dentreprise-open-source-de-vmware-part-1/>